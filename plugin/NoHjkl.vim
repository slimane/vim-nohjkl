try
    command! -nargs=0 NoHjklStart :call NoHjkl#startNoHjkl()
    command! -nargs=0 NoHjklStop  :call NoHjkl#endNoHjkl()

    if exists("g:NoHjklAutoRun") ==? 1
        NoHjklStart
    endif
    autocmd! BufEnter * if exists( 'g:NoHJKLEnable' ) && g:NoHJKLEnable == 1
    \                   |   call NoHjkl#startNoHjkl()
    \                   | elseif exists('g:NoHJKLEnable') && g:NoHJKLEnable == 0
    \                   |   call NoHjkl#endNoHjkl()
    \                   | endif

catch /E117.*/
    echoerr 'this plugin required : KeyMap.vim( https://bitbucket.org/slimane/vim-keymap )'
endtry
