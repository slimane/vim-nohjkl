" flag ==?  1 is true
" flag !=? 1 is false
function! NoHjkl#startNoHjkl()
    let g:NoHJKLEnable = 1


    if ( exists('b:maplist'))
        return
    endif


    let l:mapList = []
    for l:map in ['h', 'j', 'k', 'l'
    \           , '<up>', '<down>', '<right>', '<left>']
        call add(l:mapList, keymap#Object#New(l:map, 'noremap').createMap())
        call add(l:mapList, keymap#Object#New(l:map, 'noremap!').createMap())
    endfor

    let b:maplist = filter(l:mapList, 'v:val  != ""')


    noremap  <buffer>h       <nop>
    noremap  <buffer>j       <nop>
    noremap  <buffer>k       <nop>
    noremap  <buffer>l       <nop>
    noremap! <buffer><up>    <nop>
    noremap! <buffer><down>  <nop>
    noremap! <buffer><right> <nop>
    noremap! <buffer><left>  <nop>

endfunction

function! NoHjkl#endNoHjkl()
    let g:NoHJKLEnable = 0

    if !exists('b:maplist')
        return
    endif


    try
        unmap  <buffer>h
        unmap  <buffer>j
        unmap  <buffer>k
        unmap  <buffer>l
        unmap! <buffer><up>
        unmap! <buffer><down>
        unmap! <buffer><right>
        unmap! <buffer><left>
    catch /E31*/
    " 特に問題のないエラーのため握りつぶす
    endtry


    for mapStr in b:maplist
        execute mapstr
    endfor

    unlet b:maplist
endfunction
